<?php

namespace App\Definitions;

class EmployeeDefinition
{
    const DEPARTMENTId = 'department_id';
    const USERID = 'user_id';
    const SALARY = 'salary';
    const ROLEID = 'role_id';
}
