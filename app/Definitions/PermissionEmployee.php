<?php

namespace App\Definitions;

class PermissionEmployee
{
    const LIST = 'employees.list';
    const ADD = 'employees.add';
    const GET = 'employees.get';
    const EDIT = 'employees.edit';
    const DELETE = 'employees.delete';
}
