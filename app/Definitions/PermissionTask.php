<?php

namespace App\Definitions;

class PermissionTask
{
    const LIST = 'tasks.list';
    const ADD = 'tasks.add';
    const GET = 'tasks.get';
    const EDIT = 'tasks.edit';
    const DELETE = 'tasks.delete';
    const ASSIGN = 'tasks.assign';
    const CHANGESTATUS = 'tasks.change_status';
    const REVOKE = 'tasks.revoke';

}
