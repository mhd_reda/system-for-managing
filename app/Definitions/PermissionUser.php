<?php

namespace App\Definitions;

class PermissionUser
{
    const LIST = 'users.list';
    const ADD = 'users.add';
    const GET = 'users.get';
    const EDIT = 'users.edit';
    const DELETE = 'users.delete';
}
