<?php

namespace App\Definitions;

class RoleDefinition
{
    public const ADMIN = 'admin';
    public const SUB_ADMIN = 'sub_admin';
    public const EMPLOYEE = 'employee';
}
