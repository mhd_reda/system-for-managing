<?php

namespace App\Definitions;

class TaskDefinition
{
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const ENDOFDATE = 'end_of_date';
    const STATUS = 'status';
    const EMPLOYEEID = 'employee_id';
}
