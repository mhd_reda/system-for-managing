<?php

namespace App\Definitions;

class UserDefinition
{
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
}
