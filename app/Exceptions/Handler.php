<?php

namespace App\Exceptions;

use App\Common\SharedMessage;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param Request $request
     * @param Throwable $exception
     * @return JsonResponse|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        // return parent::render($request, $exception);

        if ($exception instanceof ModelNotFoundException || $exception instanceof NotFoundHttpException) {
            return $request->expectsJson()
                ? response()->json(new SharedMessage('Requested resource is not found',[], false, 'not found', 404), 404)
                : redirect()->guest($exception->redirectTo() ?? route('not_found'));
        } else if ($exception instanceof AuthenticationException) {
            return $request->expectsJson()
                ? response()->json(new SharedMessage('Session expired',[], false, 'UnAuthenticated', 401), 401)
                : redirect()->guest($exception->redirectTo() ?? route('login'));
        } else if ($exception instanceof AuthorizationException) {
            $message = 'Unauthorized';
            $responseCode = 403;
            if ($exception->getCode())
                $responseCode = $exception->getCode();
            if ($exception->getMessage())
                $message = $exception->getMessage();

            return $request->expectsJson()
                ? response()->json(new SharedMessage($message,[], false, 'Authorization Exception', $responseCode), $responseCode)
                : redirect()->guest(route('/'));
        } else if ($exception instanceof ValidationException) {
            $message = 'The given data was invalid';
            $errors = $exception->errors();

            foreach ($errors as $key => $error) {
                $msgError = $error[0];
                $message = $msgError;
                break;
            }

            return $request->expectsJson()
                ? \response()->json(new SharedMessage($message, $errors, 'false', 'Validation Exception', 422), 422)
                : parent::render($request, $exception);
        } else if ($exception instanceof MethodNotAllowedHttpException) {

            $method = $request->method();
            $message = "The method" . " $method " . "not allowed to this http request";

            return $request->expectsJson()
                ? response()->json(new SharedMessage($message, $errors, 'false', 'Method Not Allowed Http Exception', 405), 405)
                : parent::render($request, $exception);
        } else if ($exception instanceof BadRequestHttpException) {

            $message = $exception->getMessage();

            return $request->expectsJson()
                ? response()->json(new SharedMessage($message, $errors, 'false', 'Bad Request Http Exception', 400), 400)
                : parent::render($request, $exception);
        } else if ($exception instanceof AccessDeniedHttpException) {

            $message = "Unauthorized";

            return $request->expectsJson()
                ? response()->json(new SharedMessage($message, $errors, 'false', 'Access Denied Http Exception', 403), 403)
                : parent::render($request, $exception);
        }

        return parent::render($request, $exception);
    }
}
