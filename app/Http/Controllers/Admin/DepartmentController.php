<?php

namespace App\Http\Controllers\Admin;

use App\Definitions\RoleDefinition;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\BaseCrudController;
use App\Http\Controllers\Controller;
use App\Services\DepartmentService;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class DepartmentController extends BaseController
{

    protected $service;
    /**
     * @param DepartmentService $service
     */
    public function __construct(DepartmentService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return $this->handleSharedMessage(
            $this->service->get()
        );
    }
}
