<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Models\Employee;
use App\Services\EmployeeService;
use Illuminate\Http\Request;

class EmployeeController extends BaseController
{
    protected $service;

    /**
     * __construct
     *
     * @param  mixed $service
     * @return void
     */
    public function __construct(EmployeeService $service)
    {
        $this->service = $service;
    }

    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Employee::class);

        $validated = $request->validate([
            'number' => 'required|required',
        ]);

        return $this->handleSharedMessage(
            $this->service->get($validated['number'])
        );
    }

    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(StoreEmployeeRequest $request)
    {
        return $this->handleSharedMessage(
            $this->service->create($request->all())
        );
    }


    /**
     * destroy
     *
     * @param  mixed $employee
     * @return void
     */
    public function destroy(Employee $employee)
    {
        $this->authorize('delete', $employee);
        return $this->handleSharedMessage(
            $this->service->delete($employee)
        );
    }


    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $employee
     * @return void
     */
    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        return $this->handleSharedMessage(
            $this->service->update($request->all(), $employee)
        );
    }


    /**
     * show
     *
     * @param  mixed $employee
     * @return void
     */
    public function show(Employee $employee)
    {
        $this->authorize('view', $employee);

        return $this->handleSharedMessage(
            $this->service->show($employee)
        );
    }
}
