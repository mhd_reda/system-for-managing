<?php

namespace App\Http\Controllers\Admin;

use App\Common\SharedMessage;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends BaseController
{
    public function index()
    {
        $message = new SharedMessage(
        'success',
        Role::all(),
        true,
        null,
        200);

        return $this->handleSharedMessage(
            $message
        );
    }
}
