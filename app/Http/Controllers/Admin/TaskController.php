<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\AssignTaskRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Http\Request;

class TaskController extends BaseController
{
    protected $service;

    /**
     * __construct
     *
     * @param  mixed $service
     * @return void
     */
    public function __construct(TaskService $service)
    {
        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Task::class);

        $validated = $request->validate([
            'number' => 'required|numeric',
        ]);

        return $this->handleSharedMessage(
            $this->service->get($validated['number'])
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTaskRequest $request)
    {
        return $this->handleSharedMessage(
            $this->service->create($request->all())
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        $this->authorize('view', $task);

        return $this->handleSharedMessage(
            $this->service->show($task)
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        return $this->handleSharedMessage(
            $this->service->update($request->all(), $task)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);

        return $this->handleSharedMessage(
            $this->service->delete($task)
        );
    }


    /**
     * assign
     *
     * @param  mixed $request
     * @param  mixed $task
     * @return void
     */
    public function assign(AssignTaskRequest $request, Task $task)
    {
        return $this->handleSharedMessage(
            $this->service->assign($request->all(), $task)
        );
    }


    /**
     * revoke
     *
     * @param  mixed $task
     * @return void
     */
    public function revoke(Task $task)
    {
        $this->authorize('revoke', $task);

        return $this->handleSharedMessage(
            $this->service->revoke($task)
        );
    }



    /**
     * changeStatus
     *
     * @param  mixed $request
     * @param  mixed $task
     * @return void
     */
    public function changeStatus(Request $request, Task $task)
    {
        $this->authorize('changeStatus', $task);

        $validated = $request->validate([
            'status' => 'required|in:pending,to_do,in_progress,done',
        ]);

        return $this->handleSharedMessage(
            $this->service->changeStatus($validated, $task)
        );
    }
}
