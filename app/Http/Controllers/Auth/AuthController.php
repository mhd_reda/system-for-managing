<?php

namespace App\Http\Controllers\Auth;

use App\Common\SharedMessage;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;

class AuthController extends BaseController
{
    public function login(LoginRequest $request)
    {
        $data = $request->all();

        if (!auth()->attempt($data)) {
            $message =  new SharedMessage('errors', [], false, 404);
            return $this->handleSharedMessage($message);
        }

        $token = \Auth::user()->createToken('API Token')->accessToken;
        $message =  new SharedMessage(
            'Log in success',
            ['user' => auth()->user(), 'token' => $token],
            true,
            null,
            200
        );
        return $this->handleSharedMessage($message);
    }
}
