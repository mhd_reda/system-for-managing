<?php

namespace App\Http\Controllers;

use App\Common\SharedMessage;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class BaseController extends Controller
{
    use ResponseAPI;

    /**
     * Handle manager messages.
     * @param SharedMessage $message
     * @return JsonResponse
     */
    protected function handleSharedMessage(SharedMessage $message): JsonResponse
    {
        // Check on message status.
        if ($message->status) {
            // Return success response.
            return $this->success(
                $message->data,
                $message->message,
                $message->statusCode ?? JsonResponse::HTTP_OK
            );
        }
        // Handle error of this message.
        return $this->error(
            [$message->message],
            $message->message,
            $message->statusCode ?? JsonResponse::HTTP_BAD_REQUEST
        );
    }
}
