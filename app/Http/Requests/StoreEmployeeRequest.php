<?php

namespace App\Http\Requests;

use App\Definitions\EmployeeDefinition;
use App\Definitions\UserDefinition;
use App\Models\Employee;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Employee::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            UserDefinition::EMAIL => 'required|unique:users|email',
            UserDefinition::NAME => 'required|string',
            UserDefinition::PASSWORD => 'required|string',
            EmployeeDefinition::SALARY => 'numeric',
            EmployeeDefinition::DEPARTMENTId => 'required|exists:departments,id',
            EmployeeDefinition::ROLEID => 'required|exists:roles,id'
        ];
    }
}
