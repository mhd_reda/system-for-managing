<?php

namespace App\Http\Requests;

use App\Definitions\TaskDefinition;
use App\Models\Task;
use Illuminate\Foundation\Http\FormRequest;

class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('create', Task::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            TaskDefinition::TITLE => 'required|string',
            TaskDefinition::DESCRIPTION => 'string',
            TaskDefinition::ENDOFDATE => 'required|date',
            TaskDefinition::STATUS => 'string',
            TaskDefinition::EMPLOYEEID => 'exists:employees,id'
        ];
    }
}
