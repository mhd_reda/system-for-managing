<?php

namespace App\Http\Requests;

use App\Definitions\EmployeeDefinition;
use App\Definitions\UserDefinition;
use App\Models\Employee;
use App\Services\EmployeeService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('employee'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            UserDefinition::EMAIL => [
                'required',
                'email',
                Rule::unique('users')->ignore( $this->route('employee')->user_id, 'id')
            ],
            UserDefinition::NAME => 'string',
            UserDefinition::PASSWORD => 'string',
            EmployeeDefinition::SALARY => 'numeric',
            EmployeeDefinition::DEPARTMENTId => 'exists:departments,id',
            EmployeeDefinition::ROLEID => 'exists:roles,id',
            'id' => 'require|exists:employees,id'
        ];
    }
}
