<?php

namespace App\Http\Requests;

use App\Definitions\TaskDefinition;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('task'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            TaskDefinition::TITLE => 'string',
            TaskDefinition::DESCRIPTION => 'string',
            TaskDefinition::ENDOFDATE => 'date',
            TaskDefinition::STATUS => 'string',
            TaskDefinition::EMPLOYEEID => 'exists:employees,id'
        ];
    }
}
