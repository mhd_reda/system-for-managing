<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Definitions\DepartmentDefinition;
use App\Definitions\EmployeeDefinition;

class Department extends Model
{
    use HasFactory;

    protected $fillable = [DepartmentDefinition::NAME];

    public function Employee()
    {
        return $this->hasMany(Employee::class, EmployeeDefinition::DEPARTMENTId);
    }
}
