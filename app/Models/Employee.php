<?php

namespace App\Models;

use App\Definitions\EmployeeDefinition;
use App\Definitions\TaskDefinition;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [EmployeeDefinition::SALARY, EmployeeDefinition::USERID, EmployeeDefinition::DEPARTMENTId];

    public function Department()
    {
        return $this->belongsTo(Department::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Task()
    {
        return $this->hasMany(Task::class, TaskDefinition::EMPLOYEEID);
    }

}
