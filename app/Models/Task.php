<?php

namespace App\Models;

use App\Definitions\TaskDefinition;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [TaskDefinition::TITLE, TaskDefinition::DESCRIPTION,
    TaskDefinition::ENDOFDATE, TaskDefinition::STATUS, TaskDefinition::EMPLOYEEID];

    public function Employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
