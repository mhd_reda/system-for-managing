<?php

namespace App\Policies;

use App\Definitions\PermissionEmployee;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(PermissionEmployee::LIST);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo(PermissionEmployee::ADD);
    }

    /**
     * @param User $user
     * @param Employee $employee
     * @return bool
     */
    public function view(User $user, Employee $employee)
    {
        return $user->hasPermissionTo(PermissionEmployee::GET);
    }

    /**
     * @param User $user
     * @param Employee $employee
     * @return bool
     */
    public function update(User $user, Employee $employee)
    {
        return $user->hasPermissionTo(PermissionEmployee::EDIT);
    }

    /**
     * @param User $user
     * @param Employee $employee
     * @return bool
     */
    public function delete(User $user, Employee $employee)
    {
        return $user->hasPermissionTo(PermissionEmployee::DELETE);
    }
}
