<?php

namespace App\Policies;

use App\Definitions\PermissionTask;
use App\Models\Task;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo(PermissionTask::LIST);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo(PermissionTask::ADD);
    }

    /**
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function view(User $user, Task $task)
    {
        return $user->hasPermissionTo(PermissionTask::GET);
    }

    /**
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function update(User $user, Task $task)
    {
        return $user->hasPermissionTo(PermissionTask::EDIT);
    }

    /**
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function delete(User $user, Task $task)
    {
        return $user->hasPermissionTo(PermissionTask::DELETE);
    }

     /**
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function assign(User $user, Task $task)
    {
        return $user->hasPermissionTo(PermissionTask::ASSIGN);
    }

     /**
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function revoke(User $user, Task $task)
    {
        return $user->hasPermissionTo(PermissionTask::REVOKE);
    }

     /**
     * @param User $user
     * @param Task $task
     * @return bool
     */
    public function changeStatus(User $user, Task $task)
    {
        return $user->hasPermissionTo(PermissionTask::CHANGESTATUS);
    }
}
