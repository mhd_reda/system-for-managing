<?php

namespace App\Repositories;

use App\Models\Department;


class DepartmentRepository
{
    public function get()
    {
        return Department::query()->get();
    }
}
