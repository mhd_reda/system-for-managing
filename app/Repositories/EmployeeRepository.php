<?php

namespace App\Repositories;

use App\Models\Employee;

class EmployeeRepository
{

    protected $userRepository;

    /**
     * __construct
     *
     * @param  mixed $userRepository
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * get
     *
     * @param  mixed $data
     * @return void
     */
    public function get($data)
    {
        $employees = Employee::query()->with('user')->paginate($data);
        return $employees;
    }


    /**
     * create
     *
     * @param  mixed $data
     * @return void
     */
    public function create($data)
    {
        return $employees = Employee::create($data);
    }


    /**
     * delete
     *
     * @param  mixed $employee
     * @return void
     */
    public function delete($employee)
    {
        $this->userRepository->delete($employee->user_id);
        return $employee->delete();
    }


    /**
     * update
     *
     * @param  mixed $data
     * @return void
     */
    public function update($data, $employee)
    {
        $this->userRepository->update($employee->user_id, $data);
        return $employee->update($data);
    }
}
