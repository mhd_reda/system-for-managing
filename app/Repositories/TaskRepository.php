<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\Task;

class TaskRepository
{
    /**
     * get
     *
     * @param  mixed $data
     */
    public function get($data)
    {
        return Task::query()->paginate($data);
    }


    /**
     * getToEmployee
     *
     * @param  mixed $data
     * @param  mixed $employee_id
     * @return void
     */
    public function getToEmployee($data, $employee_id)
    {
        return Task::query()->where('employee_id', $employee_id)->paginate($data);
    }


    /**
     * create
     *
     * @param  mixed $data
     * @return void
     */
    public function create($data)
    {
        return Task::create($data);
    }



    /**
     * delete
     *
     * @param  mixed $task
     * @return void
     */
    public function delete($task)
    {
        return $task->delete();
    }


    /**
     * update
     *
     * @param  mixed $data
     * @return void
     */
    public function update($data, $task)
    {
        return $task->update($data);
    }



    /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        return Task::find($id)->first();
    }


    /**
     * assign
     *
     * @param  mixed $data
     * @param  mixed $task
     * @return void
     */
    public function assign($data, $task)
    {
        return $task->update(['employee_id' => $data['employee_id']]);
    }


    /**
     * revoke
     *
     * @param  mixed $task
     * @return void
     */
    public function revoke($task)
    {
        return $task->update(['employee_id' => null]);
    }


    public function changeStatus($data, $task)
    {
        return $task->update($data);
    }
}
