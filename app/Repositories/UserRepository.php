<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    /**
     * create
     *
     * @param  mixed $data
     * @return void
     */
    public function create($data)
    {
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        if ($user) {
            $user->assignRole($data['role_id']);
            return $user->id;
        } else
            return false;
    }



    /**
     * delete
     *
     * @param  mixed $id
     * @return void
     */
    public function delete($id)
    {
        $user = User::find($id);
        if ($user) {
            return $user->delete();
        } else
            return "User not found";
    }


    /**
     * update
     *
     * @param  mixed $id
     * @return void
     */
    public function update($id, $data)
    {
        $user = User::find($id);
        if ($user) {
            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            }
            if (isset($data['role_id'])) {
                $user->roles()->detach(); // remove past role
                $user->assignRole($data['role_id']);
            }
            return $user->update($data);
        } else {
            return "User not found";
        }
    }
}
