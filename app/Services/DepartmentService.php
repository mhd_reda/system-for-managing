<?php

namespace App\Services;

use App\Common\SharedMessage;
use App\Repositories\DepartmentRepository;

class DepartmentService
{
    /**
     * __construct
     *
     * @param  mixed $repository
     * @return void
     */
    public function __construct(DepartmentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * get
     *
     * @return void
     */
    public function get()
    {
        return new SharedMessage(
            'success',
            $this->repository->get(),
            true,
            null,
            200
        );
    }
}
