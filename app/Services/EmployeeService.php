<?php

namespace App\Services;

use App\Common\SharedMessage;
use App\Models\Employee;
use App\Repositories\EmployeeRepository;
use App\Repositories\UserRepository;

class EmployeeService
{

    protected $repository;
    protected $userRepository;

    /**
     * __construct
     *
     * @param  mixed $repository
     * @param  mixed $userRepository
     * @return void
     */
    public function __construct(EmployeeRepository $repository, UserRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    /**
     * get
     *
     * @param  mixed $data
     * @return void
     */
    public function get($data)
    {
        return new SharedMessage(
            'success',
            $this->repository->get($data),
            true,
            null,
            200
        );
    }

    /**
     * create
     *
     * @param  mixed $data
     * @return void
     */
    public function create($data)
    {
        $data['user_id'] = $this->userRepository->create($data);
        if ($data['user_id']) {
            return new SharedMessage(
                'success',
                $this->repository->create($data),
                true,
                null,
                200
            );
        } else {
            return new SharedMessage('errors', [], false, 404);
        }
    }

    /**
     * delete
     *
     * @param  mixed $employee
     * @return void
     */
    public function delete($employee)
    {
        return new SharedMessage(
            'success',
            $this->repository->delete($employee),
            true,
            null,
            200
        );
    }


    /**
     * update
     *
     * @param  mixed $data
     * @param  mixed $employee
     * @return void
     */
    public function update($data, $employee)
    {
        return new SharedMessage(
            'success',
            $this->repository->update($data, $employee),
            true,
            null,
            200
        );
    }


    /**
     * show
     *
     * @param  mixed $employee
     * @return void
     */
    public function show($employee)
    {
        return new SharedMessage(
            'success',
            $employee,
            true,
            null,
            200
        );
    }
}
