<?php

namespace App\Services;

use App\Common\SharedMessage;
use App\Definitions\RoleDefinition;
use App\Repositories\TaskRepository;

class TaskService
{

    protected $repository;

    /**
     * __construct
     *
     * @param  mixed $repository
     * @return void
     */
    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * get
     *
     * @param  mixed $data
     */
    public function get($data)
    {
        if (auth()->user()->getRoleNames()->first() == RoleDefinition::EMPLOYEE) {
            $employee_id = auth()->user()->employee[0]['id'] ?? null;
            return new SharedMessage(
                'success',
                $this->repository->getToEmployee($data, $employee_id),
                true,
                null,
                200
            );
        }
        return new SharedMessage(
            'success',
            $this->repository->get($data),
            true,
            null,
            200
        );
    }

    /**
     * create
     *
     * @param  mixed $data
     * @return void
     */
    public function create($data)
    {

        return new SharedMessage(
            'success',
            $this->repository->create($data),
            true,
            null,
            200
        );
    }

    /**
     * delete
     *
     * @param  mixed $task
     * @return void
     */
    public function delete($task)
    {
        return new SharedMessage(
            'success',
            $this->repository->delete($task),
            true,
            null,
            200
        );
    }


    /**
     * update
     *
     * @param  mixed $data
     * @param  mixed $task
     * @return void
     */
    public function update($data, $task)
    {
        return new SharedMessage(
            'success',
            $this->repository->update($data, $task),
            true,
            null,
            200
        );
    }

    /**
     * show
     *
     * @param  mixed $task
     * @return void
     */
    public function show($task)
    {
        if (auth()->user()->getRoleNames()->first() == RoleDefinition::EMPLOYEE) {
            $employee_id = auth()->user()->employee[0]['id'] ?? null;
            if ($task->employee_id != $employee_id) {
                return new SharedMessage('unauthorized', [], false, 401);
            }
        }
        return new SharedMessage(
            'success',
            $task,
            true,
            null,
            200
        );
    }

    /**
     * assign
     *
     * @param  mixed $date
     * @param  mixed $task
     * @return void
     */
    public function assign($data, $task)
    {
        return new SharedMessage(
            'success',
            $this->repository->assign($data, $task),
            true,
            null,
            200
        );
    }


    /**
     * assign
     *
     * @param  mixed $data
     * @param  mixed $task
     * @return void
     */
    public function revoke($task)
    {
        return new SharedMessage(
            'success',
            $this->repository->revoke($task),
            true,
            null,
            200
        );
    }

    /**
     * changeStatus
     *
     * @param  mixed $task
     * @return void
     */
    public function changeStatus($data, $task)
    {
        if (auth()->user()->getRoleNames()->first() == RoleDefinition::EMPLOYEE) {
            $employee_id = auth()->user()->employee[0]['id'] ?? null;
            if ($task->employee_id != $employee_id) {
                return new SharedMessage('unauthorized', [], false, 401);
            }
        }
        return new SharedMessage(
            'success',
            $this->repository->changeStatus($data, $task),
            true,
            null,
            200
        );
    }
}
