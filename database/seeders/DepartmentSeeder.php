<?php

namespace Database\Seeders;

use App\Helpers\DepartmentHelper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => DepartmentHelper::HR,
        ]);

        DB::table('departments')->insert([
            'name' => DepartmentHelper::IT,
        ]);

        DB::table('departments')->insert([
            'name' => DepartmentHelper::Marketing,
        ]);
    }
}
