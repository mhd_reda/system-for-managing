<?php

namespace Database\Seeders;

use App\Definitions\PermissionDepartment;
use App\Definitions\PermissionEmployee;
use App\Definitions\PermissionTask;
use App\Definitions\PermissionUser;
use App\Definitions\RoleDefinition;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $subAdminRole = Role::where('name', RoleDefinition::SUB_ADMIN )->first();
        $employeeRole = Role::where('name', RoleDefinition::EMPLOYEE)->first();

        $permission = Permission::create(['name' => PermissionDepartment::LIST, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);

        $permission = Permission::create(['name' => PermissionEmployee::LIST, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);

        $permission = Permission::create(['name' => PermissionEmployee::ADD, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionEmployee::GET, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);
        $permission = Permission::create(['name' => PermissionEmployee::EDIT, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionEmployee::DELETE, 'guard_name' => 'api']);


        $permission = Permission::create(['name' => PermissionTask::LIST, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);
        $permission->assignRole($employeeRole);
        $permission = Permission::create(['name' => PermissionTask::ADD, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionTask::GET, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);
        $permission->assignRole($employeeRole);
        $permission = Permission::create(['name' => PermissionTask::EDIT, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);
        $permission = Permission::create(['name' => PermissionTask::DELETE, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionTask::ASSIGN, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);
        $permission = Permission::create(['name' => PermissionTask::CHANGESTATUS, 'guard_name' => 'api']);
        $permission->assignRole($employeeRole);
        $permission = Permission::create(['name' => PermissionTask::REVOKE, 'guard_name' => 'api']);
        $permission->assignRole($subAdminRole);

        $permission = Permission::create(['name' => PermissionUser::LIST, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionUser::ADD, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionUser::GET, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionUser::EDIT, 'guard_name' => 'api']);
        $permission = Permission::create(['name' => PermissionUser::DELETE, 'guard_name' => 'api']);
    }
}
