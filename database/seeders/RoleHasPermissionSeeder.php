<?php

namespace Database\Seeders;

use App\Definitions\RoleDefinition;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleHasPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', RoleDefinition::ADMIN)->first();
        $allPermission = Permission::all();

        $adminRole->syncPermissions($allPermission);

    }
}
