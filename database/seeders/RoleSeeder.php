<?php

namespace Database\Seeders;

use App\Definitions\RoleDefinition;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => RoleDefinition::ADMIN, 'guard_name' => 'api']);
        Role::create(['name' => RoleDefinition::SUB_ADMIN, 'guard_name' => 'api']);
        Role::create(['name' => RoleDefinition::EMPLOYEE, 'guard_name' => 'api']);

    }
}
