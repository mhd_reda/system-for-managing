<?php

namespace Database\Seeders;

use App\Definitions\RoleDefinition;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = DB::table('users')->where('id', 1)->first();
        if (!$admin) {
            $user = User::create([
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('12345678'),
            ]);
            $user->assignRole(RoleDefinition::ADMIN);
        }
    }
}
