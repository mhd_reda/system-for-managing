<?php

use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\EmployeeController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\TaskController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class,'login']);

Route::middleware(['auth:api'])->group(function () {

    Route::get('/get-departments', [DepartmentController::class,'index']);
    Route::resource('employees', EmployeeController::class);
    Route::resource('tasks', TaskController::class);
    Route::post('/assign-task/{task}', [TaskController::class,'assign']);
    Route::post('/revoke-task/{task}', [TaskController::class,'revoke']);
    Route::post('/change-status-task/{task}', [TaskController::class,'changeStatus']);
    Route::get('roles', [RoleController::class, 'index']);
});

